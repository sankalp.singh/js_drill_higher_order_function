// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument
// based off http://underscorejs.org/#each

function each(items, cb) {
	if (
		typeof items === "undefined" ||
		typeof cb === "undefined" ||
		!(cb instanceof Function)
	) {
		return undefined;
	} else {
		for (let index = 0; index < items.length; index++) {
			cb(items[index], index);
		}
		return cb;
	}
}
module.exports = each;
