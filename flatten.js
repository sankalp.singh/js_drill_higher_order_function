/* Flattens a nested array (the nesting can be to any depth).
 Hint: You can solve this using recursion.
 Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
 */
function flatten(elements, newElements = []) {
	if (typeof elements === "undefined") {
		return [];
	} else {
		for (let index = 0; index < elements.length; index++) {
			if (Array.isArray(elements[index])) {
				flatten(elements[index], newElements);
			} else {
				newElements.push(elements[index]);
			}
		}
		return newElements;
	}
}
module.exports = flatten;
