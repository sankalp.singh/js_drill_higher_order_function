/* Do NOT use .filter, to complete this function.
 Similar to `find` but you will return an array of all elements that passed the truth test
 Return an empty array if no elements pass the truth test
*/
function filter(elements, cb) {
	let newElements = [];
	if (typeof elements === "undefined" || !(cb instanceof Function)) {
		return [];
	} else {
		for (let index = 0; index < elements.length; index++) {
			if (cb(elements[index])) {
				newElements.push(elements[index]);
			}
		}
		return newElements;
	}
}
module.exports = filter;
