const each = require("../each");
const items = [1, 2, 3, 4, 5, 5];
// const items = [];

each(items, (a, b) => {
	console.log(a + " : " + b);
});
