const reduce = require("../reduce");
// const items = [1, 2, 3, 4, 5, 5];
const items = [];

console.log(
	reduce(
		items,
		(a, b) => {
			return a + b;
		},
		100
	)
);
